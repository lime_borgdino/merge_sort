
function merge(a,p,r,q) {
    let n1 = q - p + 1
    let n2 = r - q
    let left = []
    let right = []

    console.log(n1)
    console.log(n2)
    console.log(q)
    console.log(p)
    console.log(r)
    
    for (let i = 0; i < n1; i++) {
        left[i] = a[p + i]
    }
    for (let j = 0; j < n2; j++) {
        right[j] = a[q + j + 1]
    }
    
    let i = 0
    let j = 0
    let k = p

    while (i < n1 && j < n2) {
        if(left[i] <= right[j]) {
            a[k] = left[i]
            i++
        } else {
            a[k] = right[j]
            j++
        }
        k++
    }

    while (i < n1) {
        a[k] = left[i]
        i++
        k++
    }

    while (j < n2) {
        a[k] = right[j]
        j++
        k++
    }
}

function mergeSort(a,p,r) {
    if (p < r) {
        let q = Math.floor((p + r) / 2)
        mergeSort(a,p,q)
        mergeSort(a,q + 1,r)
        merge(a,p,q,r)
    }
}

let x = [2,8,7,1,5,3]
let n = x.length - 1
mergeSort(x,0,5)
console.log(x)